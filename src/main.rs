#![allow(dead_code)]
#![allow(unused)]
#![allow(unused_variables)]

use std::env;
use std::fs;
use std::string::String;
use std::{
    fs::File,
    io::{prelude::*, BufReader},
};

pub enum Orientation {
    Nord,
    Est,
    Ouest,
    Sud,
}
#[derive(Debug)]
struct Point{
    x: i32,
    y: i32,
}

struct Point{
    x: i32,
    y: i32,
}

struct Robot{
    p:Point,
    orientation: Orientation,
    instructions: Vec<char>,
}

fn this_can_fail(bool: bool)->bool{

if bool == false{

return false;

}
true
}

fn main() {

    let filename: String = "two_robots.txt".to_string();

    let contents = match fs::read_to_string(filename){
        Ok(val) => val,
        Err(err) => return,
    };

    println!("With text:\n{}", contents);
    let mut xmax: usize  = 0;
    let mut ymax: usize = 0;

    let mut elem = contents.split_whitespace();


    if let Some(x) = elem.next() {
        xmax = xmax + x.parse::<usize>().unwrap();
    }

    if let Some(x) = elem.next() {
        ymax = ymax + x.parse::<usize>().unwrap();
    }

    println!("{:?}", xmax);
    println!("{:?}", ymax);


    let mut n = 1;

    let mut robot = Robot{p:Point{x : 0, y : 0}, orientation : Orientation::Nord, instructions: Vec::new()};

    let stockrobot: Vec<Robot>;

    for encours in elem{

        match n{
            1 => robot.p.x = encours.parse::<i32>().unwrap(),
            2 => robot.p.y = encours.parse::<i32>().unwrap(),
            3 => robot.orientation = conversion(encours.to_string()),
            4 => robot.instructions = encours.chars().collect(),
            5 => {stockrobot.push(robot); break} // rentrer ce robot dans la structure qui contient tout les robots
            _ => (),
        }
        n += 1;
        if n == 5{ 
            n = 1;
            let mut robot = Robot{p:Point{x : 0, y : 0}, orientation : Orientation::Nord, instructions: Vec::new()};
        }
    }

    let mut tab: Vec<Point> = Vec::new();

    for o in stockrobot.iter_mut(){
        for i in o.instructions.clone(){
            match i{
                'L' => left(o),
                'R' => right(o),
                'F' => {forward(o); tab.push(Point{x: o.p.x, y: o.p.y});},
                _ => break,
            }
            println!("truc");
        }
        println!("truc2");
    }
    println!("{:?}", tab);

}

fn conversion(x: String) -> Orientation {
    if x == "N".to_string(){
        return Orientation::Nord;
    }
    if x == "E".to_string(){
        return Orientation::Est;
    }
    if x == "S".to_string(){
        return Orientation::Sud;
    }
    if x == "O".to_string(){
        return Orientation::Ouest;
    }
return Orientation::Nord}

fn left(robot: &mut Robot){
    match &robot.orientation{
        Orientation::Nord => robot.orientation = Orientation::Ouest,
        Orientation::Est => robot.orientation = Orientation::Nord,
        Orientation::Sud => robot.orientation = Orientation::Est,
        Orientation::Ouest => robot.orientation = Orientation::Sud,
    }
}

fn right(robot: &mut Robot){
    match &robot.orientation{
        Orientation::Nord => robot.orientation = Orientation::Est,
        Orientation::Est => robot.orientation = Orientation::Sud,
        Orientation::Sud => robot.orientation = Orientation::Ouest,
        Orientation::Ouest => robot.orientation = Orientation::Nord,
    }
}

fn forward(robot: &mut Robot){
    match &robot.orientation{
        Orientation::Nord => robot.p.y -= 1,
        Orientation::Est => robot.p.x += 1,
        Orientation::Sud => robot.p.y += 1,
        Orientation::Ouest => robot.p.x -= 1,
    }
}
#[cfg(test)]
mod tests {
    #[test]
    fn is_n_works(){
        assert!(is_nord(Direction::Nord), true);
        assert!(is_nord(Direction::Sud),false);
    }
}
